from copy import deepcopy

import utils
from dataset import constants


base_config = 'NOT_USED'


def gen_multi_and_single_tasks(config_template):
    if '%' not in config_template:
        raise ValueError("Template value missing")

    multi_name = config_template.replace('%', 'Multi')
    single_name = config_template.replace('%', 'Single')
    return [
        {
            'base_config': multi_name,
            'argv': '--dataset v1',
            'exp_desc': 'base_v1',
            'eval_on': ['v1']
        },
        *[
            {
                'base_config': single_name,
                'argv': f'--dataset v1 {utils.traits_string(trait)}',
                'exp_desc': f'base_{trait}_v1',
                'eval_on': ['v1']
            }
            for trait in constants.TRAITS
        ],
    ]


experiments = gen_multi_and_single_tasks('config/Thi%Task.yml')

# experiments = [
#     {
#         'base_config': 'config/ThiMultiTask.yml',
#         'argv': '--dataset v1',
#         'exp_desc': 'base_v1',
#         'eval_on': ['v1']
#     },
#     *[
#         {
#             'base_config': 'config/ThiSingleTask.yml',
#             'argv': f'--dataset v1 {utils.traits_string(trait)}',
#             'exp_desc': f'base_{trait}_v1',
#             'eval_on': ['v1']
#         }
#         for trait in constants.TRAITS
#     ],
# ]
# no_trim_experiments = []
# for exp in experiments:
#     exp = deepcopy(exp)
#     no_trim_experiments.append(exp)
# experiments += no_trim_experiments

experiments += gen_multi_and_single_tasks('config/Thi%TaskNoVL.yml')
# experiments += gen_multi_and_single_tasks('config/Thi%TaskNoLRShift.yml')
experiments += gen_multi_and_single_tasks('config/Thi%TaskNoHuber.yml')
experiments += gen_multi_and_single_tasks('config/Thi%TaskNoDropout.yml')

experiments += gen_multi_and_single_tasks('config/MLP%Task.yml')
experiments += gen_multi_and_single_tasks('config/LSTM%Task.yml')

# experiments += [
#     {
#         'base_config': 'config/MLPMultiTask.yml',
#         'argv': '--dataset v1',
#         'exp_desc': 'base_v1',
#         'eval_on': ['v1']
#     },
#     *[
#         {
#             'base_config': 'config/MLPSingleTask.yml',
#             'argv': f'--dataset v1 {utils.traits_string(trait)}',
#             'exp_desc': f'base_{trait}_v1',
#             'eval_on': ['v1']
#         }
#         for trait in constants.TRAITS
#     ],
#     {
#         'base_config': 'config/LSTMMultiTask.yml',
#         'argv': '--dataset v1',
#         'exp_desc': 'base_v1',
#         'eval_on': ['v1']
#     },
#     *[
#         {
#             'base_config': 'config/LSTMSingleTask.yml',
#             'argv': f'--dataset v1 {utils.traits_string(trait)}',
#             'exp_desc': f'base_{trait}_v1',
#             'eval_on': ['v1']
#         }
#         for trait in constants.TRAITS
#     ]
# ]

# Add duplicate experiments where we're testing on the full v2 dataset
test_experiments = []
for exp in experiments:
    exp = deepcopy(exp)
    exp['argv'] += ' --test_dataset all'
    exp['eval_on'] = ['v2']
    test_experiments.append(exp)

experiments += test_experiments

experiments += [
    {
        "pls": True,
    },
    {
        "xgb": True,
    }
]

utils.run_experiments(base_config, experiments, num_parallel=2)

"""
These are required for paper experiments:
python3 train_PLS_Regression.py --dataset v1 --exp_desc pls/base_v1 --test_dataset all
python3 train_XGB.py --dataset v1 --exp_desc xgb/base_v1 --test_dataset all

These are for the server:
IMPORTANT: Don't forget to alter HyperSpectrumDataSet:46 to actually limit the spectral range
python3 train_PLS_Regression.py --dataset v1 --exp_desc pls/base_v1 --test_dataset all
python3 train_PLS_Regression.py --dataset v1 --exp_desc pls/base_v1_400-900 --test_dataset all
python3 train_PLS_Regression.py --dataset v1 --exp_desc pls/base_v1_400-1000 --test_dataset all
python3 train_PLS_Regression.py --dataset v1 --exp_desc pls/base_v1_400-1700 --test_dataset all

Then, run `bundle_models.py` to copy all of the model weights to where the server can access them.
"""
