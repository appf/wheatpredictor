#!/bin/bash
# Select traits
python3 train_PLS_Regression.py - -dataset v1 - -exp_desc pls_thing / base - -traits \[\"LMA_O\", \"SPAD_O\"\]
python3 train_PLS_Regression.py - -dataset v2 - -exp_desc pls_thing / base_v2 - -traits \[\"LMA_O\", \"SPAD_O\"\]
python3 train_PLS_Regression.py - -dataset mex2 - -exp_desc pls_thing / base_mex2 - -traits \[\"LMA_O\", \"SPAD_O\"\]
python3 train_PLS_Regression.py - -dataset y19 - -exp_desc pls_thing / base_y19 - -traits \[\"LMA_O\", \"SPAD_O\"\]
python3 train_PLS_Regression.py - -dataset dwarf19 - -exp_desc pls_thing / base_dwarf19 - -traits \[\"LMA_O\", \"SPAD_O\"\]
python3 train_PLS_Regression.py - -dataset v1 + v2 + y19 + mex2 - -exp_desc pls_thing / base_v1 + v2 + y19 + mex2 - -traits \[\"LMA_O\", \"SPAD_O\"\]

python3 train_PLS_Regression.py - -dataset v1 + v2 + y19 + mex2 - -exp_desc pls_thing_temp / base_v1 + v2 + y19 + mex2 - -traits \[\"SPAD_O\"\]

# All traits
python3 train_PLS_Regression.py - -dataset v1 - -exp_desc pls_thing / base
python3 train_PLS_Regression.py - -dataset v2 - -exp_desc pls_thing / base_v2
python3 train_PLS_Regression.py - -dataset mex2 - -exp_desc pls_thing / base_mex2
python3 train_PLS_Regression.py - -dataset y19 - -exp_desc pls_thing / base_y19
python3 train_PLS_Regression.py - -dataset dwarf19 - -exp_desc pls_thing / base_dwarf19
python3 train_PLS_Regression.py - -dataset v1 + v2 + y19 + mex2 - -exp_desc pls_thing / base_v1 + v2 + y19 + mex2

# Unused
python3 train_PLS_Regression.py - -dataset v1 - -exp_desc pls / base_v1
python3 train_PLS_Regression.py - -dataset v2 - -exp_desc pls_thing / base_v2
python3 train_PLS_Regression.py - -dataset mex2 - -exp_desc pls_thing / base_mex2
python3 train_PLS_Regression.py - -dataset y19 - -exp_desc pls_thing / base_y19
python3 train_PLS_Regression.py - -dataset dwarf19 - -exp_desc pls_thing / base_dwarf19
python3 train_PLS_Regression.py - -dataset v1 - -exp_desc pls_thing / base_v1_Aus - -data_filter "('col_val',{'column_name':'Exp','value_match_str':'Aus'})"
python3 train_PLS_Regression.py - -dataset v1 - -exp_desc pls_thing / base_v1_Mex - -data_filter "('col_val',{'column_name':'Exp','value_match_str':'Mex'})"
python3 train_PLS_Regression.py - -dataset v1 + v2 + y19 + mex2 - -exp_desc pls_thing / base_v1 + v2 + y19 + mex2


# Don't forget to alter HyperSpectrumDataSet:46 to actually limit the spectral range
python3 train_PLS_Regression.py - -dataset v1 + v2 + y19 + mex2 - -exp_desc pls / base_v1 + v2 + y19 + mex2_400 - 900
python3 train_PLS_Regression.py - -dataset v1 + v2 + y19 + mex2 - -exp_desc pls / base_v1 + v2 + y19 + mex2_400 - 1000
python3 train_PLS_Regression.py - -dataset v1 + v2 + y19 + mex2 - -exp_desc pls / base_v1 + v2 + y19 + mex2_400 - 1700
python3 train_PLS_Regression.py - -dataset v1 + v2 + y19 + mex2 - -exp_desc pls / base_v1 + v2 + y19 + mex2_400 - 2400


cp .. / results / pls / base_v1 + v2 + y19 + mex2_400 - 900 / models.bin .. / data / model_weights / pls_400 - 900.bin
cp .. / results / pls / base_v1 + v2 + y19 + mex2_400 - 1000 / models.bin .. / data / model_weights / pls_400 - 1000.bin
cp .. / results / pls / base_v1 + v2 + y19 + mex2_400 - 1700 / models.bin .. / data / model_weights / pls_400 - 1700.bin
cp .. / results / pls / base_v1 + v2 + y19 + mex2_400 - 2400 / models.bin .. / data / model_weights / pls_400 - 2400.bin
