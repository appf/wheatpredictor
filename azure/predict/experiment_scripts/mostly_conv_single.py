import utils


base_config = 'config/MostlyConv.yml'


experiments = []
for trait in ['SPAD_O', 'LMA_O']:
    experiments.append({
        'argv': f'--dataset v1+v2+y19+mex2 --CNN_interm_channels 32 {utils.traits_string(trait)}',
        'exp_desc': f'base_32chan_{trait}_v1+v2+y19+mex2',
        'all_data': True
    })

utils.run_experiments(base_config, experiments, num_parallel=2)
