import os
import numpy as np
import pandas as pd

# 0.7 train, 0.1 validation and 0.2 test
SPLIT_RATIOS = [.7, .1, .2]


DATA_DIR = "/data"
DATAFRAME_DIR = os.path.join(DATA_DIR, 'processed')
SPLIT_DIR = os.path.join(DATA_DIR, 'splits')

DATASET_VERSION = 'v2'

in_path = os.path.join(DATAFRAME_DIR, f'dataset_{DATASET_VERSION}.csv')

traits = [
    "LMA_O",
    "Narea_O",
    "SPAD_O",
    "Nmass_O",
    "Parea_O",
    "Pmass_O",
    "Vcmax",
    "Vcmax25",
    "J",
    "Photo_O",
    "Cond_O",
    "Vcmax25_Narea_O"
]

if DATASET_VERSION == 'v2':
    BLACKLIST = ['Parea_O', 'Pmass_O']
    traits = [t for t in traits if t not in BLACKLIST]


def get_train_val_test_split(data_file, trait):
    df = pd.read_csv(data_file)
    df['col_index'] = range(0, len(df))

    filtered_trait_NA = df[df[trait].notnull()]

    train = filtered_trait_NA.sample(frac=SPLIT_RATIOS[0])
    val = filtered_trait_NA.drop(train.index).sample(frac=(SPLIT_RATIOS[1] / (1 - SPLIT_RATIOS[0])))
    test = filtered_trait_NA.drop(train.index).drop(val.index)

    return train['col_index'].values, val['col_index'].values, test['col_index'].values


def save(trait, train, val, test):
    print("Trait:", trait)
    print("\tTrain size:", len(train))
    print("\tVal size:", len(val))
    print("\tTest size:", len(test))

    np.savez(os.path.join(SPLIT_DIR, f'split_{trait}_{DATASET_VERSION}.npz'), train=train, val=val, test=test)


for trait in traits:
    np.random.seed(42)
    print("Processing:", in_path, trait)
    train, val, test = get_train_val_test_split(in_path, trait)
    save(trait, train, val, test)
print("Done!")
