"""Experiment runner which launches and manages parallel experiments"""
import dataset.constants as constants
import dataset.utils as utils
import os
import sys
import shutil
import time
import subprocess

sys.path.append('.')


DATASET_VERSIONS = utils.FILE_PATHS.keys()


def data_filter_str(match):
    return '--data_filter "(\'col_val\',{\'column_name\':\'Exp\',\'value_match_str\':\'' + match + '\'})"'


def inv_data_filter_str(match):
    return '--data_filter "(\'inv_col_val\',{\'column_name\':\'Exp\',\'value_match_str\':\'' + match + '\'})"'


def traits_string(traits):
    if isinstance(traits, str):
        traits = [traits]
    for trait in traits:
        if trait not in constants.TRAITS:
            raise ValueError(f"Never heard of the trait \"{trait}\" before!")

    traits_str = ','.join(['\\"' + t + '\\"' for t in traits])
    return '--traits [' + traits_str + ']'


def _run_pls_experiment():
    p = subprocess.Popen("python3 train_PLS_Regression.py --dataset v1 --exp_desc pls/base_v1 --test_dataset all", shell=True)
    return p


def _run_xgb_experiment():
    p = subprocess.Popen("python3 train_XGB.py --dataset v1 --exp_desc xgb/base_v1 --test_dataset all", shell=True)
    return p


def _run_experiment(config, exp_desc, eval_on, argv="", overwrite=False, mex_aus_split=False, minus_n_split=False, all_data=False):
    base_dir = os.path.splitext(os.path.split(config)[1])[0]
    full_desc = os.path.join(base_dir, exp_desc)

    if overwrite:
        shutil.rmtree(os.path.join('..', 'results', full_desc), ignore_errors=True)

    all_tasks = []
    # Train
    task_str = f"python3 train_model_tn.py --config_file {config} --exp_desc {full_desc} {argv}"
    if len(eval_on) > 0:
        task_str += " && "

    # Evaluate all the datasets
    for v in eval_on:
        all_tasks.append(f"python3 eval_model.py --config_file {config} --exp_desc {full_desc} {argv} --dataset {v} --data_filter ''")

    if minus_n_split:
        all_tasks.append(f"python3 eval_model.py --config_file {config} --exp_desc {full_desc} {argv} --dataset v1 " + data_filter_str('-N'))
        all_tasks.append(f"python3 eval_model.py --config_file {config} --exp_desc {full_desc} {argv} --dataset v1 " + inv_data_filter_str('-N'))
    if mex_aus_split:
        all_tasks.append(f"python3 eval_model.py --config_file {config} --exp_desc {full_desc} {argv} --dataset v1 " + data_filter_str('Aus'))
        all_tasks.append(f"python3 eval_model.py --config_file {config} --exp_desc {full_desc} {argv} --dataset v1 " + data_filter_str('Mex'))
    if all_data:
        all_tasks.append(f"python3 eval_model.py --config_file {config} --exp_desc {full_desc} {argv} --dataset v1+v2+y19+mex2 --data_filter ''")

    task_str += " ; ".join(all_tasks)

    p = subprocess.Popen(task_str, shell=True)
    return p


def _maybe_launch_experiments(base_config, active_experiments, remaining_experiments, num_parallel):
    """Launch some of the remaining experiments if some of the active experiments have finished"""
    active_experiments = [e for e in active_experiments if e.poll() is None]
    missing_count = min(num_parallel - len(active_experiments), len(remaining_experiments))

    if missing_count > 0:
        print("Starting {} experiments...".format(missing_count))

        new_experiments = remaining_experiments[:missing_count]
        remaining_experiments = remaining_experiments[missing_count:]

        for i, exp in enumerate(new_experiments):
            if "pls" in exp:
                print("Experiment {}: PLS".format(i + 1))
                active_experiments.append(_run_pls_experiment())
            elif "xgb" in exp:
                print("Experiment {}: XGB".format(i + 1))
                active_experiments.append(_run_xgb_experiment())
            else:
                print("Experiment {}: {}".format(i + 1, exp['exp_desc']))
                active_experiments.append(_run_experiment(exp.get('base_config', base_config),
                                                          exp['exp_desc'],
                                                          exp.get('eval_on', DATASET_VERSIONS),
                                                          exp.get('argv', ''),
                                                          exp.get('overwrite', False),
                                                          exp.get('mex_aus_split', False),
                                                          exp.get('minus_n_split', False),
                                                          exp.get('all_data', False)))
    return active_experiments, remaining_experiments


def kill_all(ps):
    """Try killing processes the nice way, then kill all other python3 processes"""
    print("\nCleaning up processes...")
    [p.kill() for p in ps]
    subprocess.Popen("pkill -9 python3", shell=True)


def run_experiments(base_config, experiments, num_parallel=4):
    """Run a list of experiments in parallel"""

    print("=== STARTING {} EXPERIMENTS WITH {} PARALLEL ===".format(len(experiments), num_parallel))
    wait = 3
    total_exp = len(experiments)
    active_experiments = []
    remaining_experiments = experiments
    num_parallel = min(num_parallel, total_exp)

    try:
        while len(remaining_experiments) > 0 or len(active_experiments) > 0:
            active_experiments, remaining_experiments = _maybe_launch_experiments(base_config,
                                                                                  active_experiments,
                                                                                  remaining_experiments,
                                                                                  num_parallel)
            for i in range(wait):
                print("\r\t\t\t\t\t\tDone: {}; Active: {}; Remaining: {}. Checking in {}...".format(
                    total_exp - len(remaining_experiments) - len(active_experiments),
                    len(active_experiments),
                    len(remaining_experiments),
                    wait - i
                ), end='', flush=True)
                time.sleep(1)
    finally:
        kill_all(active_experiments)
