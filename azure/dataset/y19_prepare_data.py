"""Process the v2 csv data file and store it in `/data/processed`"""
import os
import pandas as pd

import constants


DATASET_VERSION = 'y19'

in_path = os.path.join(constants.RAW_DIR, DATASET_VERSION, "y19.asd.final.csv")
out_path = os.path.join(constants.DATAFRAME_DIR, f'dataset_{DATASET_VERSION}.csv')

# These traits are actually predicted - so drop their columns
drop_traits = ['LMA_O', 'SPADpred', 'N_AreaO', 'Nmass_O', 'Vcmax25', 'Vcmax25_Narea', 'J']
# SPAD just needs to be renamed
new_to_old_name = {
    'SPAD': 'SPAD_O'
}


df = pd.read_csv(in_path)
df = df.drop(columns=drop_traits)
df = df.rename(columns=new_to_old_name)
for trait in constants.TRAITS:
    if trait not in df.columns:
        print("{:<12}: not found".format(trait))
    else:
        print("{:<12}: found".format(trait))

df = constants.apply_df_filter(df)


print(df)
print("Saving...")
df.to_csv(out_path, index=False, header=True)
print("Written to", out_path)
