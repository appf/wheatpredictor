-f https://download.pytorch.org/whl/torch_stable.html
torch==1.8.1+cpu
azure-functions
pyyaml
numpy==1.23.5
pandas==2.0.3
scikit-learn==1.0.1
influxdb-client==1.18.0