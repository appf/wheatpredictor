import azure.functions as func
import os


def main(req: func.HttpRequest) -> func.HttpResponse:  # these metrics are defined over a typical 8-bit RGB image
    path = os.path.join(os.path.dirname(__file__), "debug_interface.html")
    with open(path) as f:
        return func.HttpResponse(body=f.read(), mimetype='text/html', status_code=200)
