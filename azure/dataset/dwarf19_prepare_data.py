"""Process the v2 csv data file and store it in `/data/processed`"""
import os
import pandas as pd

import constants


DATASET_VERSION = 'dwarf19'

in_path = os.path.join(constants.RAW_DIR, DATASET_VERSION, 'dfrDwarf2019.csv')
out_path = os.path.join(constants.DATAFRAME_DIR, f'dataset_{DATASET_VERSION}.csv')

drop_traits = ['SPAD_Pred', 'Nmass_O', 'N_AreaO', 'LMA_O', 'Vcmax25', 'Vcmax25_Narea', 'J', 'R_FM', 'R_DM', 'R_LA']

new_to_old_name = {
    'SPAD': 'SPAD_O',
    'LMA_g_m2': 'LMA_O',
}


def read_file(path):
    df = pd.read_csv(path)
    df = constants.apply_df_filter(df)

    return df


df = read_file(in_path)
df = df.drop(columns=drop_traits)
df = df.rename(columns=new_to_old_name)

print(df)
print("Saving...")
df.to_csv(out_path, index=False, header=True)
print("Written to", out_path)
