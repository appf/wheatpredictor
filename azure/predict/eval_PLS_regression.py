import os
import pickle

import pandas as pd
import numpy as np
from sklearn.metrics import r2_score

import config
import dataset.constants as constants
import dataset.HyperSpectrumDataSet as hsd


def convert_df(data_dict, column_name):
    df = pd.DataFrame.from_dict(data_dict, orient='index')
    df = df.rename(index=str, columns={0: column_name})
    return df


def write_out_results(result_file, results):
    total = 0.0
    for key, value in results.items():
        result_file.write("trait: " + str(key) + ", R square: " + str(np.asscalar(value)) + "\n")
        total += np.asscalar(value)
    result_file.write("Average R square: " + str(total / len(results)) + "\n")


def compute_bias(observed, predicted):
    mean_observed = np.mean(observed)
    mean_predicted = np.mean(predicted)

    bias = 100.0 * (mean_predicted - mean_observed) / mean_observed

    return bias


def compute_rep(observed, predicted):
    mean_observed = np.mean(observed)
    num = len(observed)
    minus = observed - predicted
    square = np.square(minus)
    sum = np.sum(square)
    devideLen = sum / num
    sqrt = np.sqrt(devideLen)
    rep = 100.0 * sqrt / mean_observed
    return rep


if __name__ == '__main__':
    all_traits = constants.TRAITS
    # all_traits = ["Vcmax25_Narea_O"]

    train_results = {}
    val_results = {}
    test_results = {}
    val_bias_results = {}
    val_rep_results = {}
    test_bias_results = {}
    test_rep_results = {}
    predicted_labels = {}
    train_sum = 0
    test_sum = 0

    c = config.build_config()
    result_dir = os.path.join(c.result_dir, c.exp_desc)

    result_file_name = result_dir + "/results.txt"

    if not os.path.exists(result_dir):
        raise ValueError("Experiment doesn't exist")

    print("Evaluating " + c.exp_desc + "..")

    with open(os.path.join(result_dir, 'models.bin'), 'rb') as f:
        models = pickle.load(f)

    for target_trait in all_traits:
        print("Trait:", target_trait)
        model = models[target_trait]

        train_ds = hsd.HyperSpectrumDataSet(target_trait, "train", c.data_dir, c.dataset)
        test_ds = hsd.HyperSpectrumDataSet(target_trait, "test", c.data_dir, c.dataset)

        X_test, y_test = test_ds.data_values, test_ds.data_labels

        y_test = y_test.values.ravel()

        print("n_components:", model.n_components)

        # Prediction
        test_y_pred = model.predict(X_test)

        test_y_pred = test_y_pred.ravel()

        # Calculate scores
        test_rsquare = r2_score(y_test, test_y_pred)

        # denormalize
        y_test = train_ds.denormalize(y_test)
        test_y_pred = train_ds.denormalize(test_y_pred)

        test_bias = compute_bias(y_test, test_y_pred)
        test_rep = compute_rep(y_test, test_y_pred)
        test_results[target_trait] = test_rsquare
        test_bias_results[target_trait] = test_bias
        test_rep_results[target_trait] = test_rep

        print("R-square: {:.3f}".format(test_rsquare))
        y_test_df = pd.DataFrame(y_test)
        y_test_df.rename(columns={0: 'test_y'}, inplace=True)
        test_y_pred_df = pd.DataFrame(test_y_pred)
        test_y_pred_df.rename(columns={0: 'test_y_pred'}, inplace=True)

        predicted_labels[target_trait] = pd.concat([y_test_df, test_y_pred_df], axis=1)

    # print report to file
    # result_file = open(result_file_name, "w")
    # result_file.write("-------------------------------------------------\n")
    # result_file.write("n_components: " + str(n_components) +"\n")
    # result_file.write("Here are the training data set results:\n")
    # write_out_results(result_file, train_results)
    # result_file.write("-------------------------------------------------\n")
    # result_file.write("Here are the best validation data set results:\n")
    # write_out_results(result_file, val_results)
    # result_file.write("-------------------------------------------------\n")
    # result_file.write("Here are the test data set results:\n")
    # write_out_results(result_file, test_results)
    # result_file.close()
    #
    # train_df = convert_df(train_results, "train_rsquare")
    # best_val_df = convert_df(val_results, "best_val_rsquare")
    # test_df = convert_df(test_results, "test_rsquare")
    # val_bias_df = convert_df(val_bias_results, "val_bias")
    # val_rep_df = convert_df(val_rep_results, "val_rep")
    # test_bias_df = convert_df(test_bias_results, "test_bias")
    # test_rep_df = convert_df(test_rep_results, "test_rep")
    #
    # merged_df = pd.concat([train_df, best_val_df, test_df, val_bias_df, val_rep_df, test_bias_df, test_rep_df], axis=1)
    # merged_df = merged_df.rename_axis("trait")
    #
    # results_csv = result_dir + "/results.csv"
    # merged_df.to_csv(results_csv)
    #
    # for trait in all_traits:
    #     results_csv = result_dir + "/predict_labels_"+trait+".csv"
    #     predicted_labels[trait].to_csv(results_csv, index=False)
    #
    # with open(os.path.join(result_dir, 'models.bin'), 'wb') as f:
    #   pickle.dump(models, f)
