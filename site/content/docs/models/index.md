---
title: 'Models'
date: 2021-08-03T13:50:26+10:00
draft: false
weight: 2
summary: Information about the available ML models
---

### Single-task CNN model
A multiple-regression convolutional neural network for each predicted trait. Each model is comprised of many one-dimensional convolution layers, followed by fully-connected layers.

### Multi-task CNN model

A multivariate regression convolutional neural network that predicts all trait values concurrently. Like the single-task CNN, it is comprised of many one-dimensional convolution layers, followed by fully-connected layers to make a prediction for each trait.

### PLS model

An individual partial least squares regression model per trait, which operates on the reflectance measurements alone. Due to the nature of PLS, this only works on fixed-length inputs (and as such, a few models will be made available for a selection of spectral ranges).

As the PLS model doesn't have the capacity to make predictions for variable spectral ranges, a small number of separate models have been built using commonly-encountered wavelength ranges (listed below). The uploaded reflectance data will be trimmed to match PLS model with the most-similar spectral range. If the CSV data cannot be trimmed to match a model, it will be rejected by the server.

PLS Spectral ranges:

- `[400, 900]`
- `[400, 1000]`
- `[400, 1700]`
- `[400, 2400]`


### Ensemble

An ensemble composed of the PLS, CNN-multi, and CNN-single models. Each of these models makes a trait prediction, and the ensemble prediction is computed as the mean of these. 
