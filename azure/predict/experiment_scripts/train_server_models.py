import utils
from dataset import constants

base_config = 'NOT_USED'

experiments = [
    {
        'base_config': 'config/ThiMultiTask.yml',
        'argv': '--dataset v1',
        'exp_desc': 'base_v1',
        'eval_on': ['v1']
    },
    *[
        {
            'base_config': 'config/ThiSingleTask.yml',
            'argv': f'--dataset v1 {utils.traits_string(trait)}',
            'exp_desc': f'base_{trait}_v1',
            'eval_on': ['v1']
        }
        for trait in constants.TRAITS
    ]
]

utils.run_experiments(base_config, experiments, num_parallel=4)

"""
To train the PLS models, run this command:

These are for the server:
IMPORTANT: Don't forget to alter HyperSpectrumDataSet:46 to actually limit the spectral range between experiments
python3 train_PLS_Regression.py --dataset v1 --exp_desc pls/base_v1 --test_dataset all
python3 train_PLS_Regression.py --dataset v1 --exp_desc pls/base_v1_400-900 --test_dataset all
python3 train_PLS_Regression.py --dataset v1 --exp_desc pls/base_v1_400-1000 --test_dataset all
python3 train_PLS_Regression.py --dataset v1 --exp_desc pls/base_v1_400-1700 --test_dataset all

cp ../results/pls/base_v1/models.bin ../data/model_weights/pls_400-2400.bin
cp ../results/pls/base_v1_400-900/models.bin ../data/model_weights/pls_400-900.bin
cp ../results/pls/base_v1_400-1000/models.bin ../data/model_weights/pls_400-1000.bin
cp ../results/pls/base_v1_400-1700/models.bin ../data/model_weights/pls_400-1700.bin
"""
