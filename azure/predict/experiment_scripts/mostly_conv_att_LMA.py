import utils

base_config = 'config/MostlyConvAtt.yml'


experiments = [
    {
        'argv': '--CNN_interm_channels 32 ' + utils.traits_string('LMA_O'),
        'exp_desc': 'small_LMA_O',
        'all_data': True
    },
    {
        'argv': '--dataset v2 --CNN_interm_channels 32 ' + utils.traits_string('LMA_O'),
        'exp_desc': 'small_LMA_O_v2',
        'all_data': True
    },
    {
        'argv': '--dataset mex2 --CNN_interm_channels 32 ' + utils.traits_string('LMA_O'),
        'exp_desc': 'small_LMA_O_mex2',
        'all_data': True
    },
    {
        'argv': '--dataset y19 --CNN_interm_channels 32 ' + utils.traits_string('LMA_O'),
        'exp_desc': 'small_LMA_O_y19',
        'all_data': True
    },
    {
        'argv': '--dataset dwarf19 --CNN_interm_channels 32 ' + utils.traits_string('LMA_O'),
        'exp_desc': 'small_LMA_O_dwarf19',
        'all_data': True
    },
    # {
    #     'argv': utils.data_filter_str('Aus') + '  --CNN_interm_channels 32 ' + utils.traits_string('LMA_O'),
    #     'exp_desc': 'small_LMA_O_Aus',
    #     'all_data': True
    # },
    # {
    #     'argv': utils.data_filter_str('Mex') + '  --CNN_interm_channels 32 ' + utils.traits_string('LMA_O'),
    #     'exp_desc': 'small_LMA_O_Mex',
    #     'all_data': True
    # },
    {
        'argv': '--CNN_interm_channels 32 --dataset v1+v2+y19+mex2 ' + utils.traits_string('LMA_O'),
        'exp_desc': 'small_LMA_O_v1+v2+y19+mex2',
        'all_data': True
    },
]
utils.run_experiments(base_config, experiments)
