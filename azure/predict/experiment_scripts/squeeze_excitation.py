import utils

base_config = 'config/SqueezeExcitationCNN.yml'

experiments = [
    # {
    #     'argv': '',
    #     'exp_desc': 'base',
    #     'all_data': True
    # },
    # {
    #     'argv': '--dataset v2',
    #     'exp_desc': 'base_v2',
    #     'all_data': True
    # },
    # {
    #     'argv': '--dataset mex2',
    #     'exp_desc': 'base_mex2',
    #     'all_data': True
    # },
    # {
    #     'argv': '--dataset y19',
    #     'exp_desc': 'base_y19',
    #     'all_data': True
    # },
    # {
    #     'argv': '--dataset dwarf19',
    #     'exp_desc': 'base_dwarf19',
    #     'all_data': True
    # },
    {
        'argv': '--dataset v1',
        'exp_desc': 'base_v1',
        # 'all_data': True
    },
]


utils.run_experiments(base_config, experiments, num_parallel=2)
